# Define the stages of the pipeline
stages:
  - build    # Stage to build the application
  - test     # Stage to run unit tests
  - scan     # Stage to scan for vulnerabilities
  - deploy   # Stage to deploy the application

# Set Docker Hub Environment Variables
variables:
  DOCKER_HUB_USERNAME: "your_dockerhub_username"
  DOCKER_HUB_PASSWORD: "your_dockerhub_password"

# Step 1: Building the application
build_application:
  stage: build                # This job runs in the 'build' stage
  image: python:3.9           # Use the Python 3.9 Docker image
  script:                     # List of commands to run for this job
    - apt-get update && apt-get install -y python3-dev python3-pip
    - python -m pip install --upgrade pip
    - pip install -r requirements.txt   # Install dependencies from requirements.txt
    - python manage.py migrate           # Apply migrations
    - python manage.py collectstatic     # Collect static files
  artifacts:
    paths:
      - build/               # Save the 'build/' directory as an artifact to use in later stages
      

# Step 2: Running unit tests
run_tests:
  stage: test                # This job runs in the 'test' stage
  image: python:3.9          # Use the Python 3.9 Docker image
  script:                    # List of commands to run for this job
    - apt-get update && apt-get install -y python3-dev python3-pip
    - python -m pip install --upgrade pip
    - pip install -r requirements.txt  # Install dependencies from requirements.txt
    - ls -la  # List all files and directories in the current directory (debugging step)
    - ls -la core/  # List all files and directories in the 'core/' directory (debugging step)
    - cat core/tests.py  # Display the contents of 'core/tests.py' (debugging step)
    - pytest -o junit-xml=pytest.xml core/tests.py  # Run pytest and generate JUnit XML report for tests in 'core/tests.py' file
  artifacts:
    reports:
      junit: pytest.xml       # Save test results in JUnit XML format for test reports
  dependencies:
    - build_application       # This job depends on the 'build_application' job

# Step 3: Scan the application for vulnerabilities
security_scan:
  stage: scan
  image: python:3.9
  script:
    - apt-get update && apt-get install -y python3-dev python3-pip
    - python -m pip install --upgrade pip
    - pip install bandit
    - bandit -r . -f json -o bandit_report.json
  artifacts:
    reports:
      codequality: bandit_report.json  # Update the artifact report type to 'codequality'
  dependencies:
    - build_application

# Step 4: Deploying the application to a staging server
deploy_to_staging:
  stage: deploy              # This job runs in the 'deploy' stage
  image: python:3.9          # Use the Python 3.9 Docker image
  script:                    # List of commands to run for this job
    - apt-get update -qy     # Update the package list
    - apt-get install -qy openssh-client  # Install OpenSSH client for SSH commands
    - ssh -o StrictHostKeyChecking=no -i /home/ubuntu/key.pem $STAGING_SERVER_USER@$STAGING_SERVER_IP "mkdir -p /path/to/deploy"
                              # Create the deployment directory on the server if it doesn't exist
    - scp -r -i /home/ubuntu/key.pem build/ $STAGING_SERVER_USER@$STAGING_SERVER_IP:/path/to/deploy
                              # Securely copy the 'build/' directory to the staging server
  only:
    - main                   # Only run this job on the 'main' branch
  dependencies:
    - build_application      # This job depends on the 'build_application' job
    - run_tests              # This job also depends on the 'run_tests' job

# Step 5: Build the Docker image
build_docker_image:
  stage: build               # This job runs in the 'build' stage
  image: docker:latest       # Use the latest Docker image
  services:
    - docker:dind            # Enable Docker-in-Docker service
  script:                    # List of commands to run for this job
    - docker build -t $DOCKER_HUB_USERNAME/my-ecommerce-app:latest .
                              # Build the Docker image and tag it with your Docker Hub username
  artifacts:
    paths:
      - docker_image.tar     # Save the Docker image as an artifact
  dependencies:
    - build_application      # This job depends on the 'build_application' job

# Step 6: Scan the Docker image for vulnerabilities
scan_docker_image:
  stage: scan                # This job runs in the 'scan' stage
  image: aquasec/trivy:latest  # Use the Trivy Docker image for scanning
  script:                    # List of commands to run for this job
    - trivy image $DOCKER_HUB_USERNAME/my-ecommerce-app:latest
                              # Scan the Docker image
  dependencies:
    - build_docker_image     # This job depends on the 'build_docker_image' job

# Step 7: Push the Docker image to a registry if it passes the scan
push_docker_image:
  stage: deploy              # This job runs in the 'deploy' stage
  image: docker:latest       # Use the latest Docker image
  services:
    - docker:dind            # Enable Docker-in-Docker service
  script:                    # List of commands to run for this job
    - echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USERNAME" --password-stdin
                              # Log in to Docker Hub using environment variables
    - docker push $DOCKER_HUB_USERNAME/my-ecommerce-app:latest
                              # Push the Docker image to Docker Hub
  dependencies:
    - scan_docker_image      # This job depends on the 'scan_docker_image' job
